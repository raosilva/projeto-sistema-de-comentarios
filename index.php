<?php

require 'config.php';

if(isset($_POST['nome']) && !empty($_POST['nome'])){
    $nome = $_POST['nome'];
    $comment = $_POST['comment'];

    $sql = $pdo->prepare("INSERT INTO comments SET nome = :nome, comment = :comment, date_sent = NOW()");
    $sql->bindValue(":nome", $nome);
    $sql->bindValue(":comment", $comment);
    $sql->execute();

    header("Location: index.php");
}
?>


<fieldset>
    <form method="POST">
        Nome: <input type="text" name="nome" id="nome"><br><br>
        Mensagem: <textarea name="comment" id="comment"></textarea><br><br>

        <input type="submit" value="Enviar Mensagem">
    </form>
</fieldset>
<br><br>

<?php
$sql = "SELECT * FROM comments ORDER BY date_sent DESC";
$sql = $pdo->query($sql);

if($sql->rowCount() > 0){
    foreach($sql->fetchAll() as $comment):
    ?>
<strong><?php echo $comment['nome'];?></strong> | <i><small>Enviado em:
    </small></i><small><?php echo $comment['date_sent'];?></small><br><br>
<?php echo $comment['comment'];?> <br>
<hr>

<?php
    endforeach;

} else {
    echo "Não há comentários!";
}
?>